//import
const axios = require( '../node_modules/axios' );
var tunnel = require( 'tunnel' );
var FormData = require( 'form-data' );
let loginLabel = {}

class $ASIOS
{
    //proxy
    agent = tunnel.httpsOverHttp( {
        proxy: {
            host: 'http://61.216.167.26/',
            port: 8000,
        },
    } )

    //baseUrl
    MapPinRequest = axios.create( {
        baseURL: 'http://61.216.167.26/linoes/web/api/',
        withCredentials: false,
        'Content-Type': 'application/json',
        httpsAgent: this.agent,
    } )

    //form-data
    MapPinRequestForm = axios.create( {
        baseURL: 'http://61.216.167.26/linoes/web/api/',
        withCredentials: false,
        'Content-Type': 'multipart/form-data',
        httpsAgent: this.agent,
    } )

    //各項api
    //顯示標點資料 get
    apiLogin = () => this.MapPinRequest.get( 'login/inc.php' )

    //丟資料
    apiMap = data => this.MapPinRequest.post( 'map/select.php', data );
    apiMapIconLocation = data => this.MapPinRequest.post( 'location/select.php', data );
    apiMapIconType = data => this.MapPinRequestForm.post( 'type/select.php', data );
    apiMapIconHeight = data => this.MapPinRequestForm.post( 'hight/select.php', data );
    apiMapIconGrade = data => this.MapPinRequestForm.post( 'grade/select.php', data );
    apiMapIconLocationDelete = data => this.MapPinRequestForm.post( 'location/delete.php', data );
    apiMapIconLocationadd = data => this.MapPinRequestForm.post( 'location/into.php', data );
    apiMapIconLocationUpdate = data => this.MapPinRequestForm.post( 'location/update.php', data );
    apiMapIconTypeDelete = data => this.MapPinRequestForm.post( 'type/delete.php', data );

    //應用
    async login ()
    {
        try
        {
            const login = await this.apiLogin()
            if ( login.data.status === "200" )
            {
                return login.data.login_label
            }
        } catch ( error )
        {
            console.error( error )
            return {}
        }
    }
    async getMap ()
    {
        try
        {
            const login = await this.apiLogin()
            if ( login.data.status === "200" )
            {
                loginLabel = login.data.login_label
                try
                {
                    const map = await this.apiMap( {
                        user_name: loginLabel.user_name,
                        company_ID: loginLabel.company_ID,
                        grade: loginLabel.grade,
                        login_time: loginLabel.login_time
                    } )
                    return map.data
                } catch ( error )
                {
                    console.error( error )
                    return {}
                }
            }
        } catch ( error )
        {
            console.error( error )
            return {}
        }
    }
    async getMapIconLocation ( map_id = 1 )
    {
        try
        {
            const login = await this.apiLogin()
            if ( login.data.status === "200" )
            {
                loginLabel = login.data.login_label
                try
                {
                    const mapIconLocation = await this.apiMapIconLocation( {
                        company_ID: loginLabel.company_ID,
                        grade: loginLabel.grade,
                        map_id: map_id,
                    } )
                    return mapIconLocation.data

                } catch ( error )
                {
                    console.error( error );
                    return {}
                }
            }
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
    async getMapIconType ()
    {
        try
        {
            const login = await this.apiLogin()
            if ( login.data.status === "200" )
            {
                loginLabel = login.data.login_label
                let data = new FormData();
                data.append( 'company_ID', loginLabel.company_ID );

                try
                {
                    const mapIconType = await this.apiMapIconType( data )
                    return mapIconType.data
                } catch ( error )
                {
                    console.error( error );
                    return {}
                }
            }
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }

    async getMapIconHeightNGrade ()
    {
        try
        {
            const login = await this.apiLogin()
            if ( login.data.status === "200" )
            {
                loginLabel = login.data.login_label
                let data = new FormData();
                data.append( 'company_ID', loginLabel.company_ID );

                try
                {
                    const mapIconHeight = await this.apiMapIconHeight( data )
                    const mapIconGrade = await this.apiMapIconGrade( data )
                    return [ mapIconHeight.data, mapIconGrade.data ]
                } catch ( error )
                {
                    console.error( error );
                    return {}
                }
            }
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
    async deleteMapIcon ( data )
    {
        try
        {
            const deleteMapIconRes = await this.apiMapIconLocationDelete( data )
            return deleteMapIconRes.data
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
    async updateMapIcon ( data )
    {
        try
        {
            const updateMapIconRes = await this.apiMapIconLocationUpdate( data )
            return updateMapIconRes.data
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
    async addMapIcon ( data )
    {
        try
        {
            const addMapIconRes = await this.apiMapIconLocationadd( data )
            return addMapIconRes.data
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
    async deleteMapIconType ( data )
    {
        try
        {
            const deleteMapIconTypeRes = await this.apiMapIconTypeDelete( data )
            return deleteMapIconTypeRes.data
        } catch ( error )
        {
            console.error( error );
            return {}
        }
    }
}

const $axios = new $ASIOS()
module.exports = $axios