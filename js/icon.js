var lozad = require( 'lozad' )
var imagesLoaded = require( 'imagesloaded' );
imagesLoaded.makeJQueryPlugin( $ );

let deleteId = 0;
let infoMap = true
module.exports.infoMap = true
module.exports.checkAddMap = false

class iconList
{
    constructor ( canvas, x, y, w, h, data, offset, id )
    {
        this.canvas = canvas
        this.areaStartX = x
        this.areaStartY = y
        this.iconOffsetX = offset
        this.iconStartX = this.areaStartX + 100 + this.iconOffsetX
        this.iconStartY = this.areaStartY + 50
        this.areaW = w
        this.areaH = h
        this.id = id
        this.data = data
        this.iconD = 50
        this.icon = this.canvas.createImg( data.icon, '', false, ( e ) =>
        {
            console.log( 'icon success' );
        } )
        this.icon.hide();
        this.translateNext = false
        this.translatePrev = false
    }
    show ()
    {
        this.canvas.image( this.icon, this.iconStartX, this.iconStartY, this.iconD, this.iconD );
    }
    translate ( px, py )
    {
        console.log( 'translate' );
        if (
            px > this.areaStartX + this.areaW - 100 &&
            px < this.areaStartX + this.areaW - 20 &&
            py > this.areaH + 50 &&
            py < this.areaH + 100
        )
        {
            this.iconStartX += 20
        } else if (
            px > this.areaStartX + 10 &&
            px < this.areaStartX + 90 &&
            py > this.areaH + 50 &&
            py < this.areaH + 100
        )
        {
            this.iconStartX -= 20
        }
    }
    stopTranslate ( px, py )
    {
        if (
            px > this.areaStartX + this.areaW - 100 &&
            px < this.areaStartX + this.areaW - 20 &&
            py > this.areaH + 50 &&
            py < this.areaH + 100
        )
        {
            this.translateNext = false
        } else if (
            px > this.areaStartX + 10 &&
            px < this.areaStartX + 90 &&
            py > this.areaH + 50 &&
            py < this.areaH + 100
        )
        {
            this.translatePrev = false
        }
    }
}

class IconItem
{
    constructor ( canvas, x, y, w, data, id, sX, mH, mW )
    {
        this.canvas = canvas;
        this.x = x;
        this.y = y;
        this.w = w;
        this.data = data;
        this.icon = this.canvas.createImg( data.icon, '', false, ( e ) =>
        {
            console.log( 'icon success' );
        } );
        this.icon.hide()
        this.id = id;
        this.offsetX = 0;
        this.offsetY = 0;
        this.dragging = true;
        this.rollover = false;
        this.areaStartX = sX
        this.areaH = mH
        this.areaW = mW
        //modal
        this.addMap = true
        this.editMap = false
    }

    show ( canvas, px, py )
    {
        if ( this.dragging )
        {
            this.x = px + this.offsetX;
            this.y = py + this.offsetY;
        }

        canvas.imageMode( canvas.CENTER )
        canvas.image( this.icon, this.x, this.y, this.w, this.w );
    }

    pressed ( px, py )
    {
        this.dragging = true
        this.offsetX = this.x - px;
        this.offsetY = this.y - py;
    }

    notPressed ( px, py )
    {

        if ( this.dragging )
        {
            if ( py >= this.areaH || px < this.areaStartX || px > this.areaStartX + this.areaW )
            {
                require( "./sketch.js" ).iconItem.pop()
                return
            }
        }

        if ( this.addMap )
        {
            $.fancybox.open( $( '#map_add_check' ), {
                type: 'inline',
                clickSlide: false,
                afterShow ()
                {
                    module.exports.checkAddMap = true
                    $( '#map_add_check .fancybox-close-small' ).on( 'click', function ()
                    {
                        $.fancybox.close();

                        require( './sketch' ).iconItem.pop()
                    } )
                },
                afterClose ()
                {
                    module.exports.checkAddMap = false
                }
            } );
        }

        let sliderVal = require( './comm' ).sliderVal
        let totalScale = require( './sketch' ).totalScale
        module.exports.pointX = parseFloat( ( this.x - this.areaStartX - ( this.areaW * sliderVal / 2 ) ) / totalScale ).toFixed( 2 )
        module.exports.pointY = parseFloat( ( this.y - ( this.areaH * sliderVal / 2 ) ) * -1 / totalScale ).toFixed( 2 )

        this.addMap = false
        this.dragging = false;
        setTimeout( () =>
        {
            this.editMap = true
        }, 1 )
    }

    openEditMapModal ( px, py )
    {
        let editMap = require( './comm' ).editMap
        let editIcon = require( './sketch' ).editIcon
        let checkAddMap = module.exports.checkAddMap
        if ( this.editMap && editMap && !checkAddMap && !editIcon && infoMap )
        {
            $.fancybox.close()
            $.fancybox.open( $( '#map_info' ), {
                type: 'inline',
                touch: {
                    vertical: false,
                    momentum: false
                },
                beforeShow: () =>
                {
                    infoMap = false
                    module.exports.infoMap = false
                    let sliderVal = require( './comm' ).sliderVal
                    let totalScale = require( './sketch' ).totalScale
                    deleteId = this.id
                    let x = parseFloat( ( this.x - this.areaStartX - ( this.areaW * sliderVal / 2 ) ) / totalScale ).toFixed( 2 )
                    let y = parseFloat( ( this.y - ( this.areaH * sliderVal / 2 ) ) * -1 / totalScale ).toFixed( 2 )
                    module.exports.deleteId = deleteId
                    module.exports.data = Object.assign( this.data, { point_x: x, point_y: y } )
                    $( '.map_info__icon' ).attr( 'src', this.data.icon )
                    $( '.map_info__icon' ).attr( 'alt', this.data.title )
                    $( '.map_info__title' ).text( `${ this.data.title }` )
                    $( '.map_info__sub_point' ).text( `座標： ${ x } , ${ y }` )
                    $( '.map_info__sub_height' ).text( `高度：${ this.data.height } M` )
                    $( '.map_info__sub_type' ).text( `種類： ${ this.data.type }` )
                    $( '.map_info__spec_item' ).text( `${ this.data.use_explanation }` )
                    $( '.map_info__spec_other__item' ).text( `${ this.data.remark_word }` )
                    $( '.map_info__img' ).attr( 'src', `${ this.data.photo_URL }` )
                    $( '.map_info__img' ).attr( 'alt', `${ this.data.title }` )
                    $( '.map_info__img' ).attr( 'data-src', `${ this.data.photo_URL }` )

                    $( '.map_info__video' ).attr( 'data', this.data.video_URL )
                    $( '.map_info__audio source' ).attr( 'src', this.data.audio_URL )
                    let vm = this
                    $( '.map_info__img' ).imagesLoaded()
                        .always( function ( instance )
                        {
                            console.log( 'images loaded' );
                        } )
                        .done( function ( instance )
                        {
                            console.log( 'all images successfully loaded' );
                            $( '.map_info__img' ).attr( 'src', `${ vm.data.photo_URL }` )
                        } )
                        .fail( function ()
                        {
                            console.log( 'all images loaded, at least one is broken' );
                        } )
                        .progress( function ( instance, image )
                        {
                            console.log( 'images progressed' )
                            $( '.map_info__img' ).attr( 'src', 'assets/default_info_image.png' )
                        } );
                },
                afterClose: () =>
                {
                    infoMap = true
                    module.exports.infoMap = true
                    this.editMap = true
                    module.exports.checkAddMap = false
                }
            } );
        }
    }

    delete ( px, py )
    {
        iconItem.splice( this.id, 1 )
    }

    clickedcell ( px, py )
    {
        let d = dist( this.x, this.y, px, py );
        if ( d > this.w )
        {
            return true;
        } else
        {
            return false;
        }
    }
}

function toggleControls ( a, x )
{
    console.log( a, x );
}

module.exports.iconList = iconList
module.exports.IconItem = IconItem