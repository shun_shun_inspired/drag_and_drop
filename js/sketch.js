require( '@fancyapps/fancybox' );
var Toastr = require( 'toastr' )
require( 'toastr/build/toastr.css' )
const $axios = require( './axios' )
const { iconList, IconItem } = require( "./icon.js" );

let bgc, icon2, icon, plus, arrow_right, arrow_left, cursor;
let mapWidth, mapHeight, mapStartX, mapStartY, canvasHeight, rectHeight, mapScale, inspectSize, sliderVal, addIcon = false;
let dropdown, slider;
let windowWidth = $( '#canvas_board' ).width()
let windowHeight = $( '#canvas_board' ).height()
let iconItem, icons, iconImages, iconType, locationxys, bgcControls;
module.exports.map_id = 1
module.exports.editIcon = false

const sketch = function ( p )
{

    p.preload = () =>
    {
        arrow_right = p.loadImage( "assets/arrow-right.png" );
        arrow_left = p.loadImage( "assets/arrow-left.png" );
        plus = p.createImg( `https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTs74OWY-5NQodfeM399fcW6RsslXXi5Zg2Nw&usqp=CAU`, '', '' )
        bgc = p.loadImage( "assets/default_bgc.png" );
        cursor = p.loadImage( "assets/move.svg" );

        plus.hide()

        rectHeight = 150
        mapWidth = windowWidth
        mapHeight = windowHeight
        mapStartX = ( windowWidth - mapWidth ) / 2
        mapStartY = 0
        mapScale = 1
        inspectSize = 50
        windowScale = 1
        controls = {
            view: { x: 0, y: 0, zoom: 1 },
            viewPos: { prevX: null, prevY: null, isDragging: false },
        }

        $axios.getMap().then( res =>
        {
            console.log( 'res', res );
            Object.values( res ).forEach( el =>
            {
                $( '#map_select' ).append(
                    $( "<option></option>" ).attr( "value", JSON.stringify( el ) ).text( `${ el.map_name_CH }` )
                )
            } )

            let index = localStorage.getItem( 'map_id' ) ? localStorage.getItem( 'map_id' ) : require( './sketch' ).map_id

            bgc = p.createImg( res[ index - 1 ].map_URL, '', false, () =>
            {
                console.log( 'map success' )
            } )
            bgc.hide()

            mapScale = parseFloat( res[ index - 1 ].scale )
            windowScale = ( mapWidth / parseInt( res[ index - 1 ].width ) )
            mapHeight = parseInt( res[ index - 1 ].long ) * windowScale
            module.exports.mapHeight = mapHeight
            p.windowResized( mapWidth, mapHeight + rectHeight )
            let sliderVal = require( './comm' ).sliderVal

            $( '#map_select' ).on( 'change', changeMap )
            module.exports.totalScale = mapScale * inspectSize * windowScale * sliderVal
            module.exports.map_id = res[ index - 1 ].id
            localStorage.setItem( 'map_id', res[ index - 1 ].id )
            require( './comm' ).setterController()
        } )

    }

    changeMap = () =>
    {
        let val = JSON.parse( $( '#map_select' ).val() )
        bgc = p.createImg( val.map_URL, '', false, () =>
        {
            console.log( 'new map success' )
        } )
        bgc.hide()
        mapScale = val.scale
        windowScale = ( mapWidth / parseInt( val.width ) )
        mapHeight = parseInt( val.long ) * windowScale
        let sliderVal = require( './comm' ).sliderVal
        p.windowResized( mapWidth, ( mapHeight + rectHeight ) / 2 )

        module.exports.mapHeight = mapHeight
        module.exports.totalScale = mapScale * inspectSize * windowScale * sliderVal
        module.exports.map_id = val.id
        localStorage.setItem( 'map_id', val.id )
        setupDefaultIcon( val.id )
        setupDefaultIconList()
        require( './comm' ).setterController()
    }

    let labelsList = []
    setupDefaultIconList = () =>
    {
        icons = [];
        iconImages = [];
        iconType = [];
        $axios.getMapIconType().then( res =>
        {
            if ( res.status && res.status === "200" )
            {
                labelsList = res.category
                Object.values( res.category ).forEach( ( el, idx ) =>
                {
                    iconImages.push( el )
                    iconType.push( {
                        id: el.id,
                        type: el.type
                    } )
                } );

                for ( let i = 0; i < iconImages.length; i++ )
                {
                    icons.push( new iconList( p, mapStartX, mapHeight, mapWidth, mapHeight, iconImages[ i ], 80 * i, i ) )
                }

                module.exports.iconType = iconType
                module.exports.icons = icons
            }
        } )
    }
    module.exports.setupDefaultIconList = setupDefaultIconList

    reSetupDefaultIconList = () =>
    {
        icons = [];
        iconImages = [];
        iconType = [];
        Object.values( labelsList ).forEach( ( el, idx ) =>
        {
            iconImages.push( el )
            iconType.push( {
                id: el.id,
                type: el.type
            } )
        } );

        for ( let i = 0; i < iconImages.length; i++ )
        {
            icons.push( new iconList( p, mapStartX, mapHeight, mapWidth, mapHeight, iconImages[ i ], 80 * i, i ) )
        }

        module.exports.iconType = iconType
        module.exports.icons = icons
    }

    let labels = []
    setupDefaultIcon = ( map_id ) =>
    {
        locationxys = [];
        iconItem = [];
        let sliderVal = require( './comm' ).sliderVal
        $axios.getMapIconLocation( map_id ).then( res =>
        {
            if ( res.status && res.status == 200 )
            {
                labels = res.label
                Object.values( res.label ).forEach( ( el, idx, array ) =>
                {
                    let w = mapWidth * sliderVal
                    let h = mapHeight * sliderVal
                    let X = mapStartX + ( w / 2 ) + ( parseFloat( el.locationxy.split( ',' )[ 0 ] ) * mapScale * inspectSize * windowScale * sliderVal )
                    let Y = ( h / 2 ) - ( parseFloat( el.locationxy.split( ',' )[ 1 ] ) * mapScale * inspectSize * windowScale * sliderVal )
                    iconItem.push( new IconItem( p, X, Y, 30, el, iconItem.length, mapStartX, mapHeight, mapWidth ) )
                    iconItem[ idx ].dragging = false
                    iconItem[ idx ].addMap = false
                } );
                module.exports.iconItem = iconItem
            }
        } )
    }
    module.exports.setupDefaultIcon = setupDefaultIcon

    reSetupDefaultIcon = () =>
    {
        locationxys = [];
        iconItem = [];
        let sliderVal = require( './comm' ).sliderVal
        Object.values( labels ).forEach( ( el, idx, array ) =>
        {
            let w = mapWidth * sliderVal
            let h = mapHeight * sliderVal
            let X = mapStartX + ( w / 2 ) + ( parseFloat( el.locationxy.split( ',' )[ 0 ] ) * mapScale * inspectSize * windowScale * sliderVal )
            let Y = ( h / 2 ) - ( parseFloat( el.locationxy.split( ',' )[ 1 ] ) * mapScale * inspectSize * windowScale * sliderVal )
            iconItem.push( new IconItem( p, X, Y, 30, el, iconItem.length, mapStartX, mapHeight, mapWidth ) )
            iconItem[ idx ].dragging = false
            iconItem[ idx ].addMap = false
        } );
        module.exports.totalScale = mapScale * inspectSize * windowScale * sliderVal
        module.exports.iconItem = iconItem
    }


    module.exports.reSetupDefaultIcon = reSetupDefaultIcon

    p.setup = () =>
    {
        let cnv = p.createCanvas( windowWidth, mapHeight + rectHeight );
        cnv.parent( '#canvas_board' )

        console.log( localStorage.getItem( 'map_id' ) );
        let id = localStorage.getItem( 'map_id' ) ? localStorage.getItem( 'map_id' ) : require( './sketch' ).map_id
        setupDefaultIcon( id )
        setupDefaultIconList()
    }

    p.windowResized = () =>
    {
        let sliderVal = require( './comm' ).sliderVal
        windowWidth = $( '#canvas_board' ).width()
        mapHeight = mapHeight * sliderVal * ( windowWidth / mapWidth )
        windowScale = windowScale * ( windowWidth / mapWidth )
        mapWidth = windowWidth * sliderVal
        reSetupDefaultIcon()
        reSetupDefaultIconList()
        module.exports.mapHeight = mapHeight
        require( './comm' ).setterController()
        p.resizeCanvas( windowWidth, mapHeight + rectHeight );
    }

    p.draw = () =>
    {
        p.background( 255 );

        let sliderVal = require( './comm' ).sliderVal
        let limitX = mapWidth * sliderVal / 2
        let limitY = mapHeight * sliderVal / 2
        let xc = p.constrain( controls.view.x, -limitX, limitY )
        let yc = p.constrain( controls.view.y, -limitX, limitY )
        p.push()
        p.translate( xc, yc );
        p.scale( sliderVal )
        p.image( bgc, mapStartX, mapStartY, mapWidth, mapHeight )
        p.pop()

        // icon列容器
        p.fill( 200 )
        p.rect( mapStartX, mapHeight, mapWidth, 150 )

        p.push()
        p.translate( xc, yc );
        for ( let i = 0; i < iconItem.length; i++ )
        {
            if ( i === iconItem.length - 1 && addIcon )
            {
                iconItem[ i ].show( p, p.mouseX - xc, p.mouseY - yc );
            } else
            {
                iconItem[ i ].show( p, p.mouseX, p.mouseY );
            }
        }
        p.pop()


        for ( let i = 0; i < icons.length; i++ )
        {
            icons[ i ].show();
        }

        if ( p.mouseIsPressed )
        {
            for ( let i = 0; i < icons.length; i++ )
            {
                icons[ i ].translate( p.mouseX, p.mouseY )
            }
        }

        // 左遮蔽物
        p.fill( 200 )
        p.noStroke()
        p.rect( mapStartX, mapHeight, 90, rectHeight )

        // 右遮蔽物
        p.fill( 200 )
        p.noStroke()
        p.rect( mapStartX + mapWidth - 180, mapHeight, 180, rectHeight )

        // 其他按鈕區
        p.image( plus, mapStartX + mapWidth - 170, mapHeight + 50, 50, 50 )
        p.image( cursor, mapStartX + module.exports.totalScale / sliderVal + 50, mapHeight - 85, 30, 30 )

        if ( icons.length > 7 )
        {
            p.image( arrow_right, mapStartX + mapWidth - 100, mapHeight + 50, 80, 50 )
            p.image( arrow_left, mapStartX + 10, mapHeight + 50, 80, 50 )
        }
    }

    p.mouseClicked = () =>
    {
        let sliderVal = require( './comm' ).sliderVal
        if (
            p.mouseX <= mapStartX + module.exports.totalScale / sliderVal + 100 &&
            p.mouseY <= mapHeight - 35 &&
            p.mouseX >= mapStartX + module.exports.totalScale / sliderVal + 30 &&
            p.mouseY >= mapHeight - 105
        )
        {
            if ( !controls.viewPos.isDragging )
            {
                Toastr.success( '開始移動地圖' )
            } else
            {
                Toastr.success( '關閉移動地圖' )
            }
            controls.viewPos.isDragging = !controls.viewPos.isDragging
        }

        for ( let i = 0; i < iconItem.length; i++ )
        {
            module.exports.mouseX = p.mouseX
            module.exports.mouseY = p.mouseY
            let scaleOffsetX = controls.view.x
            let scaleOffsetY = controls.view.y
            if (
                p.mouseX > iconItem[ i ].x - ( iconItem[ i ].w / 2 ) + scaleOffsetX &&
                p.mouseX < iconItem[ i ].x - ( iconItem[ i ].w / 2 ) + iconItem[ i ].w + scaleOffsetX &&
                p.mouseY > iconItem[ i ].y - ( iconItem[ i ].w / 2 ) + scaleOffsetY &&
                p.mouseY < iconItem[ i ].y - ( iconItem[ i ].w / 2 ) + iconItem[ i ].w + scaleOffsetY
            )
            {
                controls.viewPos.isDragging = false
                iconItem[ i ].openEditMapModal( p.mouseX, p.mouseY )
            }
        }

        for ( let i = 0; i < icons.length; i++ )
        {
            if (
                p.mouseX >= ( icons[ i ].iconStartX ) &&
                p.mouseX <= ( icons[ i ].iconStartX + icons[ i ].iconD ) &&
                p.mouseY >= ( icons[ i ].iconStartY ) &&
                p.mouseY <= ( icons[ i ].iconStartY + icons[ i ].iconD ) &&
                p.mouseX >= 90 &&
                p.mouseX <= mapStartX + mapWidth - 180
            )
            {

                $.fancybox.close()
                $.fancybox.open( $( '#map_icon_delete_check' ), {
                    type: 'inline',
                    touch: {
                        vertical: false,
                        momentum: false
                    },
                    beforeShow ()
                    {
                        module.exports.clickId = icons[ i ].data.id
                        module.exports.editIcon = true
                    },
                    afterClose ()
                    {
                        module.exports.editIcon = false
                    }
                } );
            }
        }

        if (
            p.mouseX >= mapStartX + mapWidth - 170 &&
            p.mouseX <= mapStartX + mapWidth - 120 &&
            p.mouseY >= mapHeight + 50 &&
            p.mouseY <= mapHeight + 100
        )
        {

            $.fancybox.close()
            $.fancybox.open( $( '#map_edit__icon' ), {
                type: 'inline',
                touch: {
                    vertical: false,
                    momentum: false
                },
                beforeShow ()
                {
                    $( 'form.map_edit__icon__from' ).attr( 'action', 'http://61.216.167.26/linoes/web/api/type/into.php' )
                    $( 'form.map_edit__icon__from input[name="company_ID"]' ).val( require( './comm' ).loginLabel.company_ID )
                    module.exports.editIcon = true
                },
                afterClose ()
                {
                    module.exports.editIcon = false
                }
            } );
        }
    }

    p.mouseDragged = () =>
    {
        const { prevX, prevY, isDragging } = controls.viewPos;
        if ( !isDragging ) return;
        if ( !require( './comm' ).editMap ) return;
        if ( !require( './icon' ).infoMap ) return;
        if (
            p.mouseX >= windowWidth - 550 &&
            p.mouseX <= windowWidth - 150 &&
            p.mouseY >= mapHeight - 150 &&
            p.mouseY <= mapHeight - 10 ||
            p.mouseY >= mapHeight ||
            p.mouseX < 0 ||
            p.mouseY < 0
        )
        {
            return
        }

        const pos = { x: p.mouseX, y: p.mouseY };
        const dx = pos.x - prevX;
        const dy = pos.y - prevY;

        if ( ( prevX || prevY ) )
        {
            $( 'body,#canvas_board' ).addClass( 'map-dragging' )
            controls.view.x += dx;
            controls.view.y += dy;
            controls.viewPos.prevX = pos.x, controls.viewPos.prevY = pos.y
        }
    }

    p.mousePressed = () =>
    {
        for ( let i = 0; i < icons.length; i++ )
        {
            if (
                p.mouseX >= ( icons[ i ].iconStartX ) &&
                p.mouseX <= ( icons[ i ].iconStartX + icons[ i ].iconD ) &&
                p.mouseY >= ( icons[ i ].iconStartY ) &&
                p.mouseY <= ( icons[ i ].iconStartY + icons[ i ].iconD ) &&
                p.mouseX >= 90 &&
                p.mouseX <= mapStartX + mapWidth - 180
            )
            {

                addIcon = true
                controls.viewPos.isDragging = false
                iconItem.push( new IconItem( p, p.mouseX, p.mouseY, 30, icons[ i ].data, iconItem.length, mapStartX, mapHeight, mapWidth ) )
            }
        }

        if ( controls.viewPos.isDragging )
        {
            controls.viewPos.prevX = p.mouseX;
            controls.viewPos.prevY = p.mouseY;
        }

    }

    p.mouseReleased = () =>
    {
        for ( let i = 0; i < iconItem.length; i++ )
        {
            iconItem[ i ].notPressed( p.mouseX, p.mouseY );
        }

        for ( let i = 0; i < icons.length; i++ )
        {
            icons[ i ].stopTranslate( p.mouseX, p.mouseY )
        }

        $( 'body,#canvas_board' ).removeClass( 'map-dragging' )
        addIcon = false
        //controls.viewPos.isDragging = true;
    }
}

let myp5 = new p5( sketch )