const $axios = require( './axios' );
const { pointX } = require( './icon' );
require( '@fancyapps/fancybox' );
var FormData = require( 'form-data' );
require( "./sketch.js" );
let loginLabel = {}
let editMap = true
module.exports.editMap = true

//comm
$( document ).ready( function ()
{

    $axios.login().then( res =>
    {
        loginLabel = res
        module.exports.loginLabel = loginLabel
    } )

    //modal
    $( '.btn-close' ).on( 'click', function ()
    {
        $.fancybox.close();

        if ( $( this ).data( 'action' ) === 0 ) return

        require( './sketch' ).iconItem.pop()
    } )

    $( '.btn-add' ).on( 'click', () =>
    {
        $.fancybox.close();
        let point_x = require( './icon.js' ).pointX
        let point_y = require( './icon.js' ).pointY
        edit( point_x, point_y )

        $( '.map_edit__from' ).attr( 'action', 'http://61.216.167.26/linoes/web/api/location/into.php' )
    } )

    $( '.btn-edit' ).on( 'click', () =>
    {
        let data = require( './icon' ).data
        $.fancybox.close();
        let point_x = require( './icon.js' ).data.point_x
        let point_y = require( './icon.js' ).data.point_y
        edit( point_x, point_y )
        $( '.map_edit__from' ).attr( 'action', 'http://61.216.167.26/linoes/web/api/location/update.php' )

        $( '.map_edit__from_title' ).val( data.title )
        $( '.map_edit__from_height' ).val( data.height )
        $( '.map_edit__from_grade' ).val( data.use_grade )
        $( '.map_edit__from_type' ).val( data.type )
        $( '.map_edit__from_use-explanation' ).val( data.use_explanation )
        $( '.map_edit__from_remark_word' ).val( data.remark_word )
        $( '.map_edit__from_file' ).removeClass( 'upload' )
        $( '.map_edit__icon__from_file__video' ).removeClass( 'upload' )
        $( '.map_edit__icon__from_file__audio' ).removeClass( 'upload' )
        $( '.map_edit__from' ).prepend( `
            <input name="id" type="hidden" readonly="readonly" value="${ data.id }">
        `)
        $( '.btn-submit' ).text( '更新' )
    } )

    $( '.btn-delete' ).on( 'click', () =>
    {

        var formdata = new FormData();
        let data = require( './icon' ).data
        formdata.append( 'id', data.id );
        formdata.append( 'company_ID', loginLabel.company_ID );
        $axios.deleteMapIcon( formdata ).then( res =>
        {
            $.fancybox.close()
            let id = localStorage.getItem( 'map_id' ) ? localStorage.getItem( 'map_id' ) : require( './sketch' ).map_id
            require( './sketch' ).setupDefaultIcon( id )
        } )
    } )

    $( '#map_icon_update' ).click( () =>
    {
        $.fancybox.close();
        $.fancybox.open( $( '#map_edit__icon' ), {
            type: 'inline',
            touch: {
                vertical: false,
                momentum: false
            },
            beforeShow ()
            {
                $( 'form.map_edit__icon__from' ).attr( 'action', 'http://61.216.167.26/linoes/web/api/type/update.php' )
                $( 'form.map_edit__icon__from input[name="company_ID"]' ).val( loginLabel.company_ID )
                $( 'form.map_edit__icon__from' ).prepend(
                    `
                    <input name="id" type="hidden" readonly="readonly" class="map_edit__icon__from_id" value="${ require( './sketch' ).clickId }">
                    `
                )
            },
            afterClose ()
            {
                $( '.map_edit__icon__from_group' ).remove()
            }
        } );
    } )

    $( '#map_icon_delete' ).click( () =>
    {
        var formdata = new FormData();
        formdata.append( 'id', require( './sketch' ).clickId );
        formdata.append( 'company_ID', loginLabel.company_ID );
        $axios.deleteMapIconType( formdata ).then( res =>
        {
            $.fancybox.close();
            require( './sketch' ).setupDefaultIconList()
        } )
    } )

    let file = '';
    let filename = '';
    $( '#map_edit__from_file' ).on( 'change', () =>
    {
        file = $( '#map_edit__from_file' ).prop( "files" )[ 0 ]
        filename = $( '#map_edit__from_file' ).prop( "files" )[ 0 ].name
        let size = ( file.size / ( 1024 * 1024 ) ).toFixed( 2 )
        if ( size >= 20 )
        {
            $( '#map_edit__from_file' ).val( null )
            alert( '圖檔大小過大' )
        } else
        {
            $( '.map_edit__from_file' ).addClass( 'upload' )
        }
    } )

    $( '#map_edit__icon__from_file' ).on( 'change', () =>
    {
        let width, height
        let _URL = window.URL || window.webkitURL;
        let file = $( '#map_edit__icon__from_file' ).prop( "files" )[ 0 ]
        let img = new Image()
        var objectUrl = _URL.createObjectURL( file );
        img.onload = function ()
        {
            width = this.width
            height = this.height
            if ( width > 300 || height > 300 )
            {
                $( '#map_edit__icon__from_file' ).val( null )
                alert( '圖片寬度與高度大小過大' )
            } else
            {
                $( '.map_edit__icon__from_file' ).addClass( 'upload' )
            }
            _URL.revokeObjectURL( objectUrl );
        };
        img.src = objectUrl;

    } )

    $( '#map_edit__icon__from_file__video' ).on( 'change', () =>
    {
        $( '.map_edit__icon__from_file__video' ).addClass( 'upload' )
    } )
    $( '#map_edit__icon__from_file__audio' ).on( 'change', () =>
    {
        $( '.map_edit__icon__from_file__audio' ).addClass( 'upload' )
    } )

    $( ".map_edit__from" ).submit( function ( e )
    {

        $.fancybox.close();

        let id = localStorage.getItem( 'map_id' ) ? localStorage.getItem( 'map_id' ) : require( './sketch' ).map_id
        $( '.loading' ).addClass( 'lds-dual-ring-bg' ).css(
            'height', `${ require( './sketch' ).mapHeight + 150 }`
        )
        setTimeout( () =>
        {
            require( './sketch' ).setupDefaultIcon( id )
            $( '.loading' ).removeClass( 'lds-dual-ring-bg' ).css(
                'height', '0'
            )
        }, 2000 )

        setTimeout( () =>
        {
            $( '.map_edit__from_title' ).val( '' )
            $( '.map_edit__from_point' ).val( '0,0' )
            $( '.map_edit__from_height' ).val( null )
            $( '.map_edit__from_grade' ).val( null )
            $( '.map_edit__from_type' ).val( null )
            $( '.map_edit__from_use-explanation' ).val( '' )
            $( '.map_edit__from_remark_word' ).val( '' )
            $( '.map_edit__from_file' ).removeClass( 'upload' )
            $( '#map_edit__icon__from_file' ).val( null )
            $( '#map_edit__icon__from_file__video' ).val( null )
            $( '#map_edit__icon__from_file__audio' ).val( null )
            $( '#map_edit__from_file' ).val( null )
        }, 600 )
    } );

    $( ".map_edit__icon__from" ).submit( ( e ) =>
    {

        $.fancybox.close();
        setTimeout( () =>
        {
            require( './sketch' ).setupDefaultIconList()
        }, 1000 )

        setTimeout( () =>
        {
            $( '.map_edit__icon__from_title' ).val( '' )
            $( '#map_edit__icon__from_file' ).val( null )
        }, 1000 )
    } )


    module.exports.sliderVal = 1
    let sliderVal = 1
    $( '#canvas_board_slider input' ).on( 'input change', () =>
    {
        sliderVal = $( '#canvas_board_slider input' ).val() * 0.1
        $( 'label[for="canvas_board_slider_input"]' ).text( '放大： ' + sliderVal.toFixed( 1 ) + '倍' )
        module.exports.sliderVal = sliderVal
        require( './sketch' ).reSetupDefaultIcon()
        setterController()
    } )

    const setterController = () =>
    {
        $( '#canvas_board_slider' ).css( {
            'top': ( require( './sketch' ).mapHeight - 100 ) + 'px',
            'right': '7%'
        } )
        $( '#canvas_board_scale' ).css( {
            'top': ( require( './sketch' ).mapHeight - 100 ) + 'px',
            'left': '180px'
        } )
        /**
         * @params totalScale 加總後的總倍率 => API給的scale * 固定50px * 圖紙放大率 ＊ 放大倍率 ＝ 1M
         * @params sliderVal 放大倍率
         * totalScale 除以 sliderVal ＝ 1 公尺
         * 因為 sliderVal 會跟著改變 所以記得除回來 才會維持長度
         */
        $( '#canvas_board_scale .line' ).css( {
            'width': require( './sketch' ).totalScale / sliderVal + 'px'
        } )
        /**
         * text({  1除以放大倍率 小數點取到第二位  })
         * @params sliderVal 放大倍率
         */
        $( '#canvas_board_scale .text' ).css( {
            'left': require( './sketch' ).totalScale / sliderVal + 30 + 'px'
        } ).text(
            `${ ( 1 / sliderVal ).toFixed( 2 ) } 公尺`
        )
    }

    module.exports.setterController = setterController
} )

function edit ( x, y, id )
{
    $axios.getMapIconHeightNGrade().then( res =>
    {
        $.fancybox.open( $( '#map_edit' ), {
            type: 'inline',
            clickSlide: false,
            touch: {
                vertical: false,
                momentum: false
            },
            beforeShow: () =>
            {
                let heights = res[ 0 ].hight_label
                let grades = res[ 1 ].hight_label
                let types = require( './sketch' ).iconType
                let optionHeights = $( '.map_edit__from_height option' ).length
                let optionGrades = $( '.map_edit__from_grade option' ).length
                let optiontypes = $( '.map_edit__from_type option' ).length

                if (
                    optionHeights < Object.keys( heights ).length &&
                    optionGrades < Object.keys( grades ).length &&
                    optiontypes < types.length
                )
                {
                    Object.values( heights ).forEach( el =>
                    {
                        $( '.map_edit__from_height' ).append(
                            $( "<option></option>" ).attr( "value", el.hight_num ).text( `${ el.hight_num } 公尺` )
                        )
                    } )

                    Object.values( grades ).forEach( el =>
                    {
                        $( '.map_edit__from_grade' ).append(
                            $( "<option></option>" ).attr( "value", el.grade_num ).text( `${ el.grade_CH }` )
                        )
                    } )

                    types.forEach( el =>
                    {
                        $( '.map_edit__from_type' ).append(
                            $( "<option></option>" ).attr( "value", el.id ).text( `${ el.type }` )
                        )
                    } )
                }

                $( 'input[name="company_ID"]' ).val( loginLabel.company_ID )
                $( 'input[name="map_id"]' ).val( require( './sketch' ).map_id )
                $( 'input[name="locationxy"]' ).val( `${ x } , ${ y }` ).attr( 'readonly', 'readonly' )

            },
            afterShow ()
            {
                editMap = false
                module.exports.editMap = false

                $( '#map_edit .btn-close' ).on( 'click', function ()
                {
                    require( './sketch' ).iconItem.pop()
                } )
                $( '#map_edit .fancybox-close-small' ).on( 'click', function ()
                {
                    require( './sketch' ).iconItem.pop()
                } )
            },
            afterClose ()
            {
                if ( $( '.map_edit__from' ).has( '.form-group-id' ) )
                {
                    $( '.form-group-id' ).remove()
                }
                editMap = true
                module.exports.editMap = true
            }
        } );
    } )
}