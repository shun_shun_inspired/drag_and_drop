var path = require("path");
var webpack = require('webpack');
const NodeExternals = require('webpack-node-externals');
const NodemonNgrokWebpackPlugin = require('nodemon-ngrok-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: process.env.NODE_ENV,
    // 指定所有entry的資料夾
    context: path.resolve(__dirname, "./"),
    // index: 'index.js', 以下為extensions所產生縮寫
    //externals: [NodeExternals()],
    entry: {
        polyfill: '@babel/polyfill',
        comm: 'comm',
        axios: 'axios',
        style: 'style',
    },
    node: {
        fs: 'empty',
        net: 'empty',
        dns: 'empty',
        tls: 'empty',
    },
    output: {
        // 依照entry的name修正output檔名
        path: path.resolve(__dirname, "dist"),
        filename: 'js/[name]_[chunkhash].js'
    },
    optimization: {
        // 將引入的套件所需的程式碼提取出來變成一個不會開啟的但又必備的JS檔或是將公用的function提取出來變成一包專用的公用檔案
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: 'vendor', //分割出來的檔案命名
                    chunks: 'initial', //把非動態模塊打包，動態模塊進行優化打包
                    enforce: true
                }
            }
        }

    },
    target: 'web',
    devServer: {
        compress: true,
        port: 3001,
        host: '0.0.0.0',
        https: false,
        disableHostCheck: true,
        stats: {
            assets: true,
            cached: false,
            chunkModules: false,
            chunkOrigins: false,
            chunks: false,
            colors: true,
            hash: false,
            modules: false,
            reasons: false,
            source: false,
            version: false,
            warnings: false
        }
    },
    plugins: [
        // 全域，不需要在各個entry獨立引入jquery
        // 建議少用providePlugin
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        // filename: output
        new MiniCssExtractPlugin({
            filename: 'css/[name]_[chunkhash].css'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
        }),

        // 原封不動copy
        new CopyWebpackPlugin([{
            from: 'assets',
            to: 'assets'
        }]),
        new CopyWebpackPlugin([{
            from: 'libs',
            to: 'libs'
        }]),

        //ngrok
        // new NodemonNgrokWebpackPlugin({
        //     ngrokConfig: {
        //         addr: 3000,
        //     },
        // })
    ],
    resolve: {
        // entry 可以省略路徑
        // 告訴webpack 解析模塊時應該搜索的目錄。
        // 絕對路徑和相對路徑都能使用
        modules: [
            path.resolve('js'),
            path.resolve('scss'),
            path.resolve('assets'),
            path.resolve('libs'),
            path.resolve('node_modules')
        ],
        //創建import或require的別名，來確保模塊引入變得更簡單
        alias: {},
        // entry 可以省略副檔名
        extensions: ['.js', '.scss']
    },
    module: {
        rules: [{
                test: /\.css$/,
                exclude: /libs/,
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                    },
                    'css-loader',
                    'postcss-loader'
                ]
            },
            {
                test: /\.(sa|sc)ss$/,
                exclude: /libs/,
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                    },
                    'css-loader',
                    'postcss-loader',
                    'resolve-url-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sourceMapContents: false
                        }
                    },
                ]
            },
            {
                test: /\.js$/,
                exclude: /bower_components/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]'
                    }
                }]
            },
            {
                // sass等有引入assets檔案時，需透過『file-loader』判別副檔案
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                    outputPath: 'fonts/'
                }
            },
            {
                test: /\.svg$/,
                exclude: /upload/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        // publicPath: './'
                    }
                }]
            }
        ]
    }
}