顯示標點資料
權限以上的資料
http://61.216.167.26/linoes/web/api/location/select.php

{
  "Label_Array": [
    {
      "id": "8",
      "locationxy": "5,5",
      "height": "1.35",
      "rotation_angle": null,
      "use_grade": "C",
      "type": "2",
      "icon": "http://61.216.167.26/linoes/img/luci/icon/2.png",
      "title": "標點01",
      "photo_URL": "http://61.216.167.26/linoes/img/luci/photo/AED.png",
      "use_explanation": "aaa",
      "remark_word": "aaa",
      "add_time": "0000-00-00 00:00:00",
      "use_time": "0000-00-00 00:00:00"
    },
    {
      "id": "10",
      "locationxy": "5,5",
      "height": "1.35",
      "rotation_angle": null,
      "use_grade": "C",
      "type": "1",
      "icon": "http://61.216.167.26/linoes/img/luci/icon/1.png",
      "title": "標點01",
      "photo_URL": "http://61.216.167.26/linoes/img/luci/photo/AED.png",
      "use_explanation": "",
      "remark_word": "aaa",
      "add_time": "0000-00-00 00:00:00",
      "use_time": "0000-00-00 00:00:00"
    },
}

---------------------------------------------------------------------------------------

更新(修改)標點資料
http://61.216.167.26/linoes/web/api/location/update.php?id=19&locationxy=2,2&height=1&grade=D&type=3&title=CCC&use_explanation=CCC&remark_word=CCC&photo_URL=CCC
id=想要修改的標點編號
locationxy=座標
height=高度(API選擇)
grade=權限(API選擇)
type=種類(API選擇)
title=標籤
use_explanation=使用說明
remark_word=備註
photo_URL=照片檔案(目前是URL->假的string)

--------------------------------------------------------------------------------------

新增標點
http://61.216.167.26/linoes/web/api/location/into.php?locationxy=5,5&height=1&grade=C&type=1&title=DDD&use_explanation=&remark_word=aaa&photo_URL=111
locationxy=座標
height=高度(API選擇)
grade=權限(API選擇)
type=種類(API選擇)
title=標籤
use_explanation=說明
remark_word=備註
photo_URL=照片檔案(目前是URL->假的string->之後會改成FTP)

--------------------------------------------------------------------------------------

刪除標點
http://61.216.167.26/linoes/web/api/location/delect.php?id=20
id=要刪除的標點編號


///////////////////////////////////////////////////////////////////////////////////////////////////

顯示種類資訊
http://61.216.167.26/linoes/web/api/type/select.php

{
  "Label_Array": [
    {
      "id": "1",
      "type": "標點",
      "icon": "http://61.216.167.26/linoes/img/luci/icon/1.png"
    },
    {
      "id": "2",
      "type": "滅火器",
      "icon": "http://61.216.167.26/linoes/img/luci/icon/2.png"
    },
  ]
}

  id =>標點編號
  type =>標點中文
  icon =>標點URL(icon圖)

--------------------------------------------------------------------------------------

新增種類
http://61.216.167.26/linoes/web/api/type/into.php?type=自定義&icon=123
type=種類中文
icon=種類的URL(目前是URL->假的string->之後會改成FTP)

--------------------------------------------------------------------------------------

更新(修改)種類
http://61.216.167.26/linoes/web/api/type/update.php?id=13&type=123&icon=456
id=想要修改的種類編號
type=種類中文
icon=種類的URL(目前是URL->假的string->之後會改成FTP)

--------------------------------------------------------------------------------------

刪除種類
http://61.216.167.26/linoes/web/api/type/delect.php?id=14
id=想要刪除的種類編號

///////////////////////////////////////////////////////////////////////////////////////////////////

顯示等級列表(下拉式選擇使用)
http://61.216.167.26/linoes/web/api/grade/select.php
[
  {
    "id": "1",
    "grade_num": "A"
  },
  {
    "id": "2",
    "grade_num": "B"
  },
  {
    "id": "3",
    "grade_num": "C"
  },
  {
    "id": "4",
    "grade_num": "D"
  }
]

///////////////////////////////////////////////////////////////////////////////////////////////////

顯示高度列表(下拉式選擇使用)
http://61.216.167.26/linoes/web/api/hight/select.php
[
  {
    "id": "1",
    "hight_num": "0.5"
  },
  {
    "id": "2",
    "hight_num": "1"
  },
  {
    "id": "3",
    "hight_num": "1.35"
  },
  {
    "id": "4",
    "hight_num": "1.5"
  },
  {
    "id": "5",
    "hight_num": "2"
  },
  {
    "id": "6",
    "hight_num": "2.5"
  },
  {
    "id": "7",
    "hight_num": "3"
  }
]




















